﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using ZRS.Library;

namespace ZRS.ConsoleApp
{
    internal class Program
    {
        static void Main(string[] args)
        {
            try
            {
                //.NET Framwerok nie obsługuje domyślnie eleganckiego systemu konfiguracji jak w nowych wersjach .NET, więc wrzuciłem BrokerAdress do App.config
                string brokerAddress = ConfigurationManager.AppSettings["BrokerAddress"];
                string topic = "zrs/test";

                using (var rng = new RNGCryptoServiceProvider())
                {
                    for (int i = 0; i < 2; i++)
                    {
                        var clientId = Guid.NewGuid().ToString();
                        Console.WriteLine($"ClientId: {clientId}");

                        using (var publisher = new MqttPublisher(brokerAddress, clientId))
                        {
                            for (int j = 0; j < 3; j++)
                            {
                                var randomBytes = new byte[4];
                                rng.GetBytes(randomBytes);
                                var randomInt32 = BitConverter.ToInt32(randomBytes, 0);

                                var mqttMessage = new MqttMessage(topic, $"Random int: {randomInt32}");
                                publisher.SendMessageExactlyOnce(mqttMessage);

                                Console.WriteLine($"Topic: {mqttMessage.Topic} Message: {mqttMessage.Message}");

                                Thread.Sleep(111);
                            }
                        }

                        Console.WriteLine();
                    }
                }

            }
            catch (Exception ex)
            {
                Console.WriteLine($"Error: {ex.Message}");
            }

            Console.Write("Press any key to continue . . .");
            Console.ReadKey();
        }
    }
}
