﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ZRS.Library
{
    public class MqttMessage
    {
        public string Topic { get; set; }
        public string Message { get; set; }

        public MqttMessage(string topic, string message)
        {
            Topic = topic;
            Message = message;
        }
    }
}
