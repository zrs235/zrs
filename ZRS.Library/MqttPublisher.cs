﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using uPLibrary.Networking.M2Mqtt.Messages;
using uPLibrary.Networking.M2Mqtt;

namespace ZRS.Library
{
    public class MqttPublisher : IDisposable
    {
        private readonly MqttClient _client;
        private bool _disposed = false;

        public string BrokerAddress { get; }
        public string ClientId { get; }

        public MqttPublisher(string brokerAddress, string clientId)
        {
            BrokerAddress = brokerAddress ?? throw new ArgumentNullException(nameof(brokerAddress));
            ClientId = clientId ?? throw new ArgumentNullException(nameof(clientId));

            _client = new MqttClient(BrokerAddress);
            var resultCode = _client.Connect(ClientId);
            CheckConnectionStatus(resultCode);
        }

        public void SendMessageExactlyOnce(MqttMessage message)
        {
            ValidateMessage(message);

            if (!_client.IsConnected)
                throw new InvalidOperationException("There is no connection to the broker.");

            _client.Publish(message.Topic, Encoding.UTF8.GetBytes(message.Message), MqttMsgBase.QOS_LEVEL_EXACTLY_ONCE, false);
        }

        private void CheckConnectionStatus(byte resultCode)
        {
            switch (resultCode)
            {
                case 0:
                    // Connection Accepted
                    break;
                case 1:
                    throw new Exception("Connection Refused: unacceptable protocol version.");
                case 2:
                    throw new Exception("Connection Refused: identifier rejected.");
                case 3:
                    throw new Exception("Connection Refused: server unavailable.");
                case 4:
                    throw new Exception("Connection Refused: bad user name or password.");
                case 5:
                    throw new Exception("Connection Refused: not authorized.");
                default:
                    throw new Exception($"Unknown connection refusal code: {resultCode}.");
            }
        }

        private void ValidateMessage(MqttMessage message)
        {
            if (message == null || string.IsNullOrWhiteSpace(message.Topic) || string.IsNullOrWhiteSpace(message.Message))
                throw new ArgumentNullException(nameof(message), "Topic and Message cannot be null or empty.");
        }



        public void Dispose()
        {
            if (_disposed)
                return;

            if (_client.IsConnected)
                _client.Disconnect();

            _disposed = true;

            GC.SuppressFinalize(this);
        }

        ~MqttPublisher()
        {
            Dispose();
        }
    }
}
